import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import TechnologyItem from './TechnologyItem'

class TechnologyList extends Component {

    render() {
        
        let items;
        if (this.props.technologyList){
            items = this.props.technologyList.map(technology => {
                return (
                    <TechnologyItem onClick={this.viewDetail} key={technology.id} technology={technology} />
                );
            });
        }

        return (
            <div className="">
                <h2>Technology List</h2>
                <div className="list-group">
                    {items}
                </div>
                <Link className="btn btn-primary" to="/technology/new">Create</Link>                
            </div>
        );
    }

    viewDetail(technologyId){
        // TODO: navigate to detailed view
        console.log(technologyId);
    }

    navigateToCreate(){
        // TODO: route to create component
    }
 }

export default TechnologyList;

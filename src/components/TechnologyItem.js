import React, { Component } from 'react';

class TechnologyItem extends Component {
    
    render() {
        return (
        <button className="list-group-item" onClick={() => this.props.onClick(this.props.technology.id)}>
            {this.props.technology.id} - {this.props.technology.description} - {this.props.technology.website}
        </button>
        );
    }
 }

export default TechnologyItem;

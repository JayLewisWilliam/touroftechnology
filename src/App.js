import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import TechnologyList from './components/TechnologyList'
import TechnologyCreate from './components/TechnologyCreate'

class App extends Component {

  componentWillMount(){
  this.setState({technologyList: [
        {
          id: "432234",
          description: "superhigh",
          website: "www.silkroad.com"
        },
        {
          id: "234233",
          description: "supercool",
          website: "www.ebay.com"
        },
        {
          id: "567576",
          description: "supercheap",
          website: "www.alibaba.com"
        },
      ]}
    );
  }

  render() {
    return (
      <Router>
        <div className="container">
         <Link to="/"></Link>
          <Route exact={true} path="/technology" render={({match}) => (
            <TechnologyList technologyList={this.state.technologyList}/>
          )} />
          <Route exact={true} path="/technology/new" render={({match}) => (
            <TechnologyCreate />
          )} />
        </div>
      </Router>
    );
  }
}

export default App;
